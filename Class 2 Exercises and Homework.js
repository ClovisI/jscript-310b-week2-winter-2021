// 1. Create an object representation of yourself
// Should include:
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)

const patrick = {
  firstName: "Patrick",
  lastName: "Gronstal",
  favouriteFood: "Steak",
  bestFriend: "Ravi"
}

const ravi = {
  firstName: "Ravi",
  lastName: "Oad",
  favouriteFood: "Chicken"
}

// 2. console.log best friend's firstName and your favorite food
console.log(ravi.firstName, patrick.favouriteFood);

// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X
const newBoard = [
  ['-','O','-'],
  ['-','X','O'],
  ['X','-','X']
];

// 4. After the array is created, 'O' claims the top right square.
// Update that value.
newBoard[0].pop(2);
newBoard[0].push('O');


// 5. Log the grid to the console.
console.log(newBoard)

// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test

const regex = new RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/);
const sampleEmail = "foo@bar.baz";

console.log(regex.test(sampleEmail));

// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';
const instanceDate = new Date(assignmentDate);

// 8. Create a new Date instance to represent the dueDate.
// This will be exactly 7 days after the assignment date.
const dueDate = new Date(instanceDate.setDate((instanceDate.getDate()+7)));

// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

const timeTag= `<time datetime="${dueDate.toISOString().slice(0,10)}">${months[0]} ${dueDate.getDate()}, ${dueDate.getFullYear()}</time>`

// 10. log this value using console.log
console.log(timeTag);
